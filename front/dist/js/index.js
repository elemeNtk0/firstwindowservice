// //
// Главный файл Js - точка сборки.
// В этом файле следует подключать другие Js файлы.
// //

$(document).ready(function() {

  $("[data-href^='#']").click(function(){var target = $(this).attr("data-href");
  $("html, body").animate({scrollTop: $(target).offset().top}, 500); return false; });

  var mainSlider = $("#js-mainSlider").lightSlider({
    adaptiveHeight: false,
    slideMargin: 0,
    item: 1,
    gallery: false,
    loop: true,
    auto: true,
    pause: 4000,
    pauseOnHover: true,
    pager: true,
    autoWidth: true,
    controls: false,
    // responsive : [],
  });

  $(".js-btnPopupShow").magnificPopup({
    removalDelay: 50,
    type:"inline",
    midClick: true
  });

  $(".js-popupForm").on("submit", function() {
    $.magnificPopup.close();
    magicForm();
  });
  $("#js-fastForm").on("submit", function() {
    magicForm();
  });

  function magicForm() {
    $("#js-textPopup").slideDown(300).delay(1500).slideUp(100);
  }





// changeCityLogic
  var $btnChangeCity = $(".js-btnChangeCity");
  var $changeCityText = $btnChangeCity.find("span").text();
  var btnValue;
  var $radioElements = $("#changeCity").find("input[type=radio]");



  $(".js-btnChangeCity").magnificPopup({
    removalDelay: 50,
    type:"inline",
    midClick: true,
    callbacks: {
      beforeClose: function() {
        for (var i = 0; i < $radioElements.length; i++) {
          if ($radioElements[i].checked) {
            changeCityBtn($radioElements[i].value);
            // console.dir($radioElements[i].value);
          }
        }
      },
    }
  });


  function changeCityBtn(value) {
    btnValue = value;
    if (btnValue == 37) $changeCityText = "Иваново";
    if (btnValue == 33) $changeCityText = "Владимир";
    if (btnValue == 44) $changeCityText = "Кострома";
    if (btnValue == 76) $changeCityText = "Ярославль";
    if (btnValue == 52) $changeCityText = "Нижний Новгород";

    $btnChangeCity.find("span").text($changeCityText);
  }

}); //.doc-ready



