// Подключаем плагины.
var
  // Ядро Gulp.
  gulp = require('gulp'),
  // Плагин, наблюдающий за изменениями в файловой системе.
  watch = require('gulp-watch'),
  // Плагин, содержащий утилиты для Gulp.
  tools = require('gulp-util'),
  // Плагин, создающий локальный веб-сервер.
  server = require('browser-sync'),
  // Плагин, очищающий содержимое папки.
  clear = require('rimraf'),
  // Плагин, компилирующий Sass.
  sass = require('gulp-sass'),
  // Плагин, сжимающий Css код.
  minCss = require('gulp-minify-css'),
  // Плагин, проставляющий префиксы (постЦСС)
  autoprefixer = require('gulp-autoprefixer'),
  // Плагин, сжимающий Js код.
  minJs = require('gulp-uglify'),
  // Плагин, позволяющий вставить в файл содержимое другого файла.
  // rigger = require('gulp-rigger'),
  // Плагин для перехвата ошибок во время построения решения.
  plumber = require('gulp-plumber'),

  rename = require("gulp-rename"),
  concat = require('gulp-concat'),
  // Функция, получающая список файлов, включая в него паттерны игнорирования.
  src = function(files) {
    var isArray = Array.isArray(files);
    if (isArray == false) files = [files];
    for (var i = 0; i < paths.ignore.length; i += 1) {
      files.push(paths.ignore[i]);
    }
    return gulp.src(files);
  };

// Настройка веб-сервера.
var config = {
  server: {baseDir: './dist'},
  tunnel: false,
  port: 9100
};

// Коллекция путей к файлам.
var paths = {
  // Пути к файлам - исходникам.
  src: {
    vendor: './src/vendor/**/*.*',
    fonts: './src/fonts/**/*.*',
    sass: './src/sass/styles.scss',
    img: './src/img/**/*.*',
    js: './src/js/index.js',
    html: './src/html/**/*.html',
  },
  // Пути к каталогам релиза проекта.
  build: {
    vendor: './dist/vendor',
    fonts: './dist/fonts',
    sass: './dist',
    img: './dist/img',
    base: './dist/**/*',
    js: './dist/js',
    html: './dist',
  },
  // Пути к файлам, изменения которых нужно отслеживать.
  watch: {
    vendor: './src/vendor/**/*.*',
    fonts: './src/fonts/**/*.*',
    sass: './src/sass/**/*.scss',
    img: './src/img/**/*.*',
    js: './src/js/**/*.js',
    html: './src/html/**/*.html',
  },
  // Маски файлов, которые следует игнорировать при построении.
  ignore: [
    '!./**/*.md'
  ]
};

// Задачи сборки.

  // Сборка Html.
  gulp.task('build:html', function() {
    src(paths.src.html)
      .pipe(plumber())
      // .pipe(rigger())
      .pipe(gulp.dest(paths.build.html))
      .pipe(server.reload({stream: true}))
    ;
  })

  // Сборка Css.
  gulp.task('build:sass', function() {
    src(paths.src.sass)
      .pipe(plumber())
      .pipe(sass())
      .pipe(autoprefixer({ browsers: ['last 3 version', 'IE 9'], cascade: false }))
      .pipe(minCss())
      .pipe(rename("template_styles.css"))
      .pipe(gulp.dest(paths.build.sass))
      .pipe(server.reload({stream: true}))
    ;
  })

  // Сборка Js.
  // gulp.task('build:js', function() {
  //  src(paths.src.js)
  //    .pipe(plumber())
  //    .pipe(rigger())
  //    .pipe(minJs())
  //    .pipe(gulp.dest(paths.build.js))
  //    .pipe(server.reload({stream: true}))
  //  ;
  // })
  gulp.task('build:js', function() {
    src(['./src/js/app/*.js', paths.src.js])
      .pipe(plumber())
      .pipe(concat('index.js'))
      // .pipe(minJs())
      .pipe(gulp.dest('./dist/js'))
      .pipe(server.reload({stream: true}))
    ;
  })

  // Сборка изображений.
  gulp.task('build:img', function() {
    src(paths.src.img)
      .pipe(plumber())
      .pipe(gulp.dest(paths.build.img))
      .pipe(server.reload({stream: true}))
    ;
  })

  // Сборка шрифтов.
  gulp.task('build:fonts', function() {
    src(paths.src.fonts)
      .pipe(plumber())
      .pipe(gulp.dest(paths.build.fonts))
      .pipe(server.reload({stream: true}))
    ;
  })

  // Сборка сторонних решений.
  // gulp.task('build:vendor', function() {
  //  src(paths.src.vendor)
  //    .pipe(plumber())
  //    .pipe(gulp.dest(paths.build.vendor))
  //    .pipe(server.reload({stream: true}))
  //  ;
  // })
  gulp.task('build:vendor', function() {
    src(['./src/vendor/jquery-1.11.3.min.js', './src/vendor/*.js'])
      .pipe(plumber())
      .pipe(concat('jquery-plugins.min.js'))
      .pipe(minJs())
      .pipe(gulp.dest('./dist/vendor/'))
      .pipe(server.reload({stream: true}))
    ;
  })
  // Сборка всего.
  gulp.task('build', ['build:html', 'build:sass', 'build:fonts', 'build:img', 'build:js', 'build:vendor'])

// Задачи наблюдения.

  // Наблюдение за Html.
  gulp.task('watch:html', function() {
    watch(paths.watch.html, function() {
      gulp.start('build:html');
    });
  })

  // Наблюдение за Js.
  gulp.task('watch:js', function() {
    watch(paths.watch.js, function() {
      gulp.start('build:js');
    });
  })

  // Наблюдение за Css.
  gulp.task('watch:sass', function() {
    watch(paths.watch.sass, function() {
      gulp.start('build:sass');
    });
  })

  // Наблюдение за изображениями.
  gulp.task('watch:img', function() {
    watch(paths.watch.img, function() {
      gulp.start('build:img');
    });
  })

  // Наблюдение за шрифтами.
  gulp.task('watch:fonts', function() {
    watch(paths.watch.fonts, function() {
      gulp.start('build:fonts');
    });
  })

  // Наблюдение за сторонними файлами.
  gulp.task('watch:vendor', function() {
    watch(paths.watch.vendor, function() {
      gulp.start('build:vendor');
    });
  })

  // Наблюдение за всем.
  gulp.task('watch', ['watch:fonts', 'watch:html', 'watch:sass', 'watch:img', 'watch:js', 'watch:vendor'])

// Прочие задачи.

  // Запуск локального сервера.
  gulp.task('server', function() {
    server(config);
  })

  // Очистка каталога релиза.
  gulp.task('clear', function(cb) {
    clear(paths.build.base, cb);
  })

// Задача по умолчанию.
gulp.task('default', ['build', 'server', 'watch'])